﻿using UnityEngine;
using System.Collections;

public class PickupWeaponScript : MonoBehaviour {

	bool canPickUp = false;

	GameObject player;

	public GameObject weapon;
	
	// Update is called once per frame
	void Update () {
		if (canPickUp) {
			if (Input.GetKeyDown (KeyCode.F)) {
				if (player != null) {
					//Spawn the weapon as a child of the MainCamera (otherwise, the weapon doesn't follow the camera).
					GameObject mainCamera = player.transform.GetChild (0).gameObject;

					GameObject weaponObject = (GameObject)Instantiate (weapon, mainCamera.transform.position, mainCamera.transform.rotation);
					weaponObject.transform.parent = mainCamera.transform;
					Destroy (this.gameObject);

				}
			}
		}

	}

	//if the player is near enough to the gun, they will be able to pick it up
	void OnTriggerEnter(Collider other){
		if (other.tag == "Player") {
			player = other.gameObject;
			Debug.Log ("You are in range now to pickup"); 
			canPickUp = true;
		}
	}
	//Once the player if far away from the gun, they will not be able to reach it
	void OnTriggerExit(Collider other){
		if(other.tag == "Player")
			canPickUp = false;
	}
}
