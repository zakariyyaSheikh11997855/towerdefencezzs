﻿using UnityEngine;

/*
 * standard public class that does not derive from mono 
 * since it does no come from mono - it needs to be serializable so 
 * unity knows how to save and use the class in the inspector
*/
[System.Serializable] 
public class BaseWeapon  {

	public string name = "Handgun"; 
	public int damage = 10;
	public float maxRange = 20f; 
	public float fireRate = 0f; //0 is for single fire wepaon, higher than that it will fire many shots. 




	public GameObject graphics;

}
