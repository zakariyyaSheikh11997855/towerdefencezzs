﻿using UnityEngine;
using System.Collections.Generic;

/*
 * Maily keeps track of the players - and also match settings
 * Since there is only one Game manger instance per scene, this has been made as a singleton
 * 
*/
public class GameManager : MonoBehaviour {

	public static GameManager singleton; 

	public MatchSettings matchsettings; 

	void Awake()
	{
		if (singleton != null) {
			Debug.LogError ("More than one Game manager is running"); 
		} else {
			singleton = this; 
			//singletons make it very easy to gain access to the Gamemanager without any gameobject.find or similar things. 
		}
	}


	/// <summary>
	/// region are easy to keep track of sections of code - the first region here is for
	/// the player code
	/// </summary>
	#region player tracking 
	private const string PLAYER_ID_PREFIX = "Player "; 

	private static Dictionary<string, Player> players = new Dictionary<string, Player>();


	/*
	 * This is so each player adds the id from network idenity to its name so that
	 * it is displayed. RegisterPlayer can be used to create a dictionary of all the
	 * players in the session
	 * 
	*/
	public static void RegisterPlayer(string _netID, Player _player)
	{
		string _playerID = PLAYER_ID_PREFIX + _netID; 
		players.Add (_playerID, _player); 
		_player.transform.name = _playerID; //sets the name of the game object to the ID
	}

	public static void DeRegisterPlayer(string _playerID)
	{
		players.Remove (_playerID);
	}

	//you can get a player from this getter
	public static Player GetPlayer(string _playerID)
	{
		return players[_playerID];
	}


	//if you want to visualise the dictionary of players it can be done from this script
	//I havent done it but if you want me to, I can work on it

	//This is just for testing - SHOULD BE COMMENTED OUT
	/*
	void OnGUI () note: found bug... the gun seems to be player 1. O.o
	{
	    GUILayout.BeginArea(new Rect(200, 200, 200, 500));
	    GUILayout.BeginVertical();

	    foreach (string _playerID in players.Keys)
	    {
	        GUILayout.Label(_playerID + "  -  " + players[_playerID].transform.name);
	    }

	    GUILayout.EndVertical();
	    GUILayout.EndArea();
	}
	*/ 

	#endregion



}
